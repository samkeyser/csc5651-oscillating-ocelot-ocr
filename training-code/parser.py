import argparse

def get_parser():
    parser = argparse.ArgumentParser()

    # argparse config options
    ## data sources
    parser.add_argument('--store-sample', default='store', help='Where to store samples')
    ### synthtext
    parser.add_argument('--synthtext-img-rootdir', default='/data/csc5651/24-1-group1/training-code/SynthText/', type=str)
    parser.add_argument('--synthtext-gt-mat', default='/data/csc5651/24-1-group1/training-code/SynthText/gt.mat', type=str)
    ### icdar2015
    parser.add_argument('--icdar2015-img-rootdir', default='/data/csc5651/24-1-group1/datasets/icdar15/', type=str)
    ### augmented emnist
    parser.add_argument("--sample_width", default=400, type=int)
    parser.add_argument("--sample_height", default=600, type=int)
    parser.add_argument("--rotation_range", default=45, type=int)
    parser.add_argument('--img_dir', default="/data/csc5651/24-1-group1/training-code/AugmentedEMNIST", type=str)
    parser.add_argument('--dataset_mat', default='/data/csc5651/24-1-group1/training-code/AugmentedEMNIST/dataset.pkl', type=str)
    parser.add_argument('--preload-emnist', action="store_true", dest="load") # set the dest to be backwards compatible with how the dataloader_emnist is set up
    ## data choice
    parser.add_argument('--synthtext-freq', type=float, default=0.25, help='% of time synthtext should be chosen')
    parser.add_argument('--icdar2015-freq', type=float, default=0.4, help='% of time icdar2015 should be chosen')
    parser.add_argument('--emnist-freq', type=float, default=0.35, help='% of time emnist dataset should be chosen')
    ## gpus
    parser.add_argument('--cuda-visible-devices', type=str, default='0', help='identifiers for the GPUs being used. Assumption should be for N GPUs assign 0..(N-1) as a comma-separated-list')
    ## target approximator
    parser.add_argument('--resume-target-approximator-from-checkpoint', action="store_true", help='load target approximator from given weight files')
    parser.add_argument('--pre-model-target-approximator', default="/data/csc5651/24-1-group1/trained-models/craft_mlt_25k.pth", type=str, help='the model to use for target approximation. By defaut, we use pretrained CRAFT (gold standard)')
    parser.add_argument('--update-target-approximator', action="store_true", help='Update the target approximator. It is held constant otherwise')
    ## model
    parser.add_argument('--resume-from-checkpoint', action="store_true", help='load models from given weight files')
    parser.add_argument('--pre-model', default="/data/csc5651/24-1-group1/trained-models/craft_mlt_25k.pth", type=str)
    ## craft
    parser.add_argument('--lara-croft', action='store_true', help='use lara croft model which substitutes LOTM modules into CRAFT')
    parser.add_argument('--use-sigmoid', action='store_true', help='use a sigmoid layer at the end of the network')
    ## hyper parameters
    parser.add_argument('--batch-size', type=int, default=4, help='input batch size')
    parser.add_argument('--lr', type=float, default=0.0001, help='learning rate for critic')
    parser.add_argument('--epochs', type=int, default=300, help='number of epochs to train for')
    parser.add_argument('--canvas_size', default=1280, type=int, help='image size for inference')
    parser.add_argument('--mag_ratio', default=1.5, type=float, help='image magnification ratio')
    ## logging
    #parser.add_argument('--displayInterval', type=int, default=100, help='Interval to be displayed')
    parser.add_argument('--save-interval', type=int, default=1000, help='interval to be saved (in number of batches)')
    parser.add_argument('--run-name', type=str, help='name of the training run, will be randomly generated if not set')

    return parser