import torch
import scipy.io as sio
import numpy as np
import cv2
import copy
import argparse
import os
import time
import torch.optim as optim
import copy
import glob
from tqdm import tqdm
from names_generator import generate_name

from craft import CRAFT
from utils import thresh_calculate, k_b_calculate, pixel_test, label_convert 
from utils import watershed_algorithm, label_generate, label_collate, averager, calculate_confidence_score

from dataloader.dataset_ic15 import ImageLoaderICDAR2015, collate_icdar2015, batch_random_augmentation
from dataloader.dataset_synthtext import ImageLoaderSynthText, collate_synthtext
from dataloader.dataset_emnist import ImageLoaderAugmentedEMNIST, collate_emnist

# Define argparser
parser = argparse.ArgumentParser()

# argparse config options
## data sources
parser.add_argument('--store-sample', default='store', help='Where to store samples')
### synthtext
parser.add_argument('--synthtext-img-rootdir', default='/data/csc5651/24-1-group1/training-code/SynthText/', type=str)
parser.add_argument('--synthtext-gt-mat', default='/data/csc5651/24-1-group1/training-code/SynthText/gt.mat', type=str)
### icdar2015
parser.add_argument('--icdar2015-img-rootdir', default='/data/csc5651/24-1-group1/datasets/icdar15/', type=str)
### augmented emnist
parser.add_argument("--sample_width", default=400, type=int)
parser.add_argument("--sample_height", default=600, type=int)
parser.add_argument("--rotation_range", default=45, type=int)
parser.add_argument('--img_dir', default="/data/csc5651/24-1-group1/training-code/AugmentedEMNIST", type=str)
parser.add_argument('--dataset_mat', default='/data/csc5651/24-1-group1/training-code/AugmentedEMNIST/dataset.pkl', type=str)
parser.add_argument('--preload-emnist', action="store_true", dest="load") # set the dest to be backwards compatible with how the dataloader_emnist is set up
## data choice
parser.add_argument('--synthtext-freq', type=float, default=0.25, help='% of time synthtext should be chosen')
parser.add_argument('--icdar2015-freq', type=float, default=0.4, help='% of time icdar2015 should be chosen')
parser.add_argument('--emnist-freq', type=float, default=0.35, help='% of time emnist dataset should be chosen')
## gpus
parser.add_argument('--cuda-visible-devices', type=str, default='0', help='identifiers for the GPUs being used. Assumption should be for N GPUs assign 0..(N-1) as a comma-separated-list')
## target approximator
parser.add_argument('--resume-target-approximator-from-checkpoint', action="store_true", help='load target approximator from given weight files')
parser.add_argument('--pre-model-target-approximator', default="/data/csc5651/24-1-group1/trained-models/craft_mlt_25k.pth", type=str, help='the model to use for target approximation. By defaut, we use pretrained CRAFT (gold standard)')
parser.add_argument('--update-target-approximator', action="store_true", help='Update the target approximator. It is held constant otherwise')
## model
parser.add_argument('--resume-from-checkpoint', action="store_true", help='load models from given weight files')
parser.add_argument('--pre-model', default="/data/csc5651/24-1-group1/trained-models/craft_mlt_25k.pth", type=str)
## craft
parser.add_argument('--lara-croft', action='store_true', help='use lara croft model which substitutes LOTM modules into CRAFT')
parser.add_argument('--use-sigmoid', action='store_true', help='use a sigmoid layer at the end of the network')
## hyper parameters
parser.add_argument('--batch-size', type=int, default=4, help='input batch size')
parser.add_argument('--lr', type=float, default=0.0001, help='learning rate for critic')
parser.add_argument('--epochs', type=int, default=300, help='number of epochs to train for')
parser.add_argument('--canvas_size', default=1280, type=int, help='image size for inference')
parser.add_argument('--mag_ratio', default=1.5, type=float, help='image magnification ratio')
## logging
#parser.add_argument('--displayInterval', type=int, default=100, help='Interval to be displayed')
parser.add_argument('--save-interval', type=int, default=1000, help='interval to be saved (in number of batches)')
parser.add_argument('--run-name', type=str, help='name of the training run, will be randomly generated if not set')

## parse out argumetns
args = parser.parse_args()

print('--------------[CHOSEN ARGS]--------------')
print(args)
print('-----------------------------------------')

# method for float equality
def epsilon_equality(a, b, epsilon=0.0001):
    return abs(a-b) < epsilon

# make sure that the chosen dataset frequencies sum to 1!
summed_freq = (args.synthtext_freq + args.icdar2015_freq + args.emnist_freq)
assert epsilon_equality(1.0, summed_freq), f'Dataset frequencies do not sum to 1, {summed_freq}'

# make sure that the sample store directory exists
print('Creating store directory', args.store_sample)
os.system('mkdir -p {0}'.format(args.store_sample))

# make sure that registry file exists
print('Creating registry file', f'{args.store_sample}/.registry')
os.system('touch {0}/.registry'.format(args.store_sample))

# load the datasets
## load synthtext
synthtext_dataset = synthtext_dataloader = None
if not epsilon_equality(args.synthtext_freq, 0.0): # no sense loading if we never want to use
    print('Loading SynthText dataloader')
    synthtext_dataset = ImageLoaderSynthText(args)
    assert synthtext_dataset
    synthtext_dataloader = torch.utils.data.DataLoader(synthtext_dataset, args.batch_size, num_workers=2, shuffle=False, collate_fn=collate_synthtext)

## load ICDAR2015
icdar2015_dataset = icdar2015_dataloader = None
if not epsilon_equality(args.icdar2015_freq, 0.0): # no sense loading if we never want to use
# TODO: refactor away from the config dictionary...
    print('Loading ICDAR2015 dataloader')
    config={'is_training':True, 'image_path': args.icdar2015_img_rootdir}
    icdar2015_dataset = ImageLoaderICDAR2015(config)
    assert icdar2015_dataset
    icdar2015_dataloader = torch.utils.data.DataLoader(icdar2015_dataset, args.batch_size, num_workers=2, shuffle=True, collate_fn=collate_icdar2015)

emnist_dataset = emnist_dataloader = None
if not epsilon_equality(args.emnist_freq, 0.0):
    print('Loading EMNIST dataloader')
    emnist_dataset = ImageLoaderAugmentedEMNIST(args)
    assert emnist_dataset
    emnist_dataloader = torch.utils.data.DataLoader(emnist_dataset, args.batch_size, num_workers=1, shuffle=True, collate_fn=collate_emnist)

# set the number of GPUs available
os.environ['CUDA_VISIBLE_DEVICES'] = args.cuda_visible_devices
print('GPUs assigned =', args.cuda_visible_devices)
print('new GPUs assigned =', os.environ.get('CUDA_VISIBLE_DEVICES', 'nil'))
gpus = list(map(int,os.environ.get('CUDA_VISIBLE_DEVICES','').split(',')))

# figure out if we're on CUDA or cpu
# getting multiple GPUs working (probably): https://stackoverflow.com/questions/67364827/only-first-gpu-is-allocated-eventhough-i-make-other-gpus-visible-in-pytorch-cu
device = f'cuda:{os.environ["CUDA_VISIBLE_DEVICES"]}' if torch.cuda.is_available() else 'cpu'
print('Running on', device)

# loss function
print('Initializing loss function')
criterion = torch.nn.MSELoss(reduction='sum').to(device)

# pull in the model
## we use a second version of craft as a target approximator for gt labels on datasets w/o char-level BBs
print('Initializing target approximator')
target_approximator = torch.nn.DataParallel(CRAFT(pretrained=True), device_ids=gpus).to(device) # note: pretrained refers to the vgg16 basenet, NOT CRAFT!

## actual model which we will be training
print('Initializing CRAFT')
craft = torch.nn.DataParallel(CRAFT(pretrained=True, use_LOTM=args.lara_croft, use_sig=args.use_sigmoid), device_ids=gpus).to(device)

## if we're resuming training, load model from checkpoint
if args.resume_from_checkpoint:
    print('loading pretrained model from %s for main model' % args.pre_model)
    craft.load_state_dict(torch.load(args.pre_model), strict=False)

## we may also want to load target approximtor, either for training or so we can use a pretrained model
if args.resume_target_approximator_from_checkpoint:
    print('loading pretrained model from %s for target approximator' % args.pre_model_target_approximator)
    target_approximator.load_state_dict(torch.load(args.pre_model_target_approximator), strict=False)

# other training thigs
loss_avg = averager()
optimizer = optim.Adam(craft.parameters(), lr=args.lr)

# training functions
## Synth
def train_batch_synthtext(data):
    craft.train()
    img, char_label, interval_label = data
    img = img.to(device)
    char_label = char_label.to(device) # region score
    interval_label = interval_label.to(device) # affinity score

    img.requires_grad_()
    optimizer.zero_grad()
    preds, _ = craft(img)
    cost_char = criterion(preds[:,:,:,0], char_label) / args.batch_size
    cost_interval = criterion(preds[:,:,:,1], interval_label) / args.batch_size
    cost = cost_char + cost_interval

    cost.backward()
    optimizer.step()
    return cost.item()

## ICDAR2015
def train_batch_icdar2015(data):
    target_approximator.eval()
    craft.train()
    img, information = data

    img = img.to(device)
    label_output, _ = target_approximator(img)
    char_label, interval_label = label_collate(label_output, information)
    #return torch.ones((1,)) # const around 4055
    
    # img, char_label, interval_label = batch_random_augmentation(img, char_label, interval_label)
    # img = img.permute(0, 3, 1, 2).to(device)
    char_label = torch.FloatTensor(char_label).to(device)
    interval_label  = torch.FloatTensor(interval_label).to(device)
    #return torch.ones((1,)) # const around 4055

    img.requires_grad_()
    optimizer.zero_grad()
    preds, _ = craft(img)

    s_conf = calculate_confidence_score(information, preds.detach())
    s_conf = torch.from_numpy(s_conf).to(device)
    #return torch.ones((1,)) # cost around 7202

    # batch x height x width x 2
    cost_char = (torch.sum(s_conf * (preds[:,:,:,0] - char_label)**2, dim=0) / args.batch_size).sum()
    cost_interval = (torch.sum(s_conf * (preds[:,:,:,1] - interval_label)**2, dim=0) / args.batch_size).sum()
    #return torch.ones((1,)) # cost around 7211

    cost = cost_char + cost_interval
    cost.backward(retain_graph=False)
    optimizer.step()
    return cost.item() # holding const around 8138

def train_on(epoch, dataloader, train_batch_fn, dataloader_name, run_name):
    batch_bar = tqdm(range(len(dataloader)), desc=f"{dataloader_name} batches")
    for i, data in zip(batch_bar,dataloader):
        # data = train_iter.next()
        cost = train_batch_fn(data)
        loss_avg.add(cost)

        # do checkpointing
        if i % args.save_interval == 0:
            torch.save(craft.state_dict(), '{0}/{1}_{2}_{3}_{4}_{5}.pth'.format(args.store_sample, 'craft' if not args.lara_croft else 'lara-croft', run_name, epoch, i, loss_avg.val()))
            
            # update the target approximator to be our current trained model
            if args.update_target_approximator:
                target_approximator = copy.deepcopy(craft)

        # update loss avg
        batch_bar.set_description(f'{dataloader_name} batches | Avg. Loss: {loss_avg.val()} | Mem. Usage: {torch.cuda.memory_allocated()/torch.cuda.max_memory_allocated()}')

        #if i % args.displayInterval == 0:
        #    print('[%d/%d][%d/%d] lr: %.6f Loss: %f Time: %f s' %
        #        (epoch, args.epochs, i, length, optimizer.param_groups[0]['lr'], loss_avg.val(), time.time()-time0))
        #    loss_avg.reset()

def get_name():
    run_name = generate_name()
    with open(os.path.join(args.store_sample, '.registry'), 'r') as datafile:
        names = set(datafile.readlines())
        while run_name in names:
            run_name = generate_name()
    return run_name

def register_name(run_name):
    with open(os.path.join(args.store_sample, '.registry'), 'a+') as datafile:
        datafile.write('{0}\n'.format(run_name))


def main():
    # generate a unique name for this run
    if not args.run_name:
        run_name = get_name()

    print("run name =", run_name)

    # register the run name so we don't use it again
    print('Registering run name')
    register_name(run_name)

    # log which model config we're running with!
    if args.lara_croft:
        print("Using LARA CROFT model!")
    else:
        print("using CRAFT model!")

                    
    # defaults -> synthtext = 0.25, icdar15 = 0.4, emnist = 0.35
    sampling_dist = [args.synthtext_freq, args.icdar2015_freq, args.emnist_freq]
    dataloaders = [
        (synthtext_dataloader, train_batch_synthtext, "SYNTHTEXT"),
        (icdar2015_dataloader, train_batch_icdar2015, "ICDAR2015"),
        (emnist_dataloader, train_batch_synthtext, "EMNIST"), # we can use the same training method as synthtext b/c we have character level gt!
    ]

    epoch_bar = tqdm(range(args.epochs), desc='epochs')
    for epoch in epoch_bar:
        # choose a dataloader according to predetermined distribution
        chosen_idx = np.random.choice(len(dataloaders), p=sampling_dist)
        chosen_dataloader, chosen_train_batch, chosen_name = dataloaders[chosen_idx]

        # perform training with chosen dataset
        train_on(epoch, chosen_dataloader, chosen_train_batch, chosen_name, run_name)

        # update progress bar
        epoch_bar.set_description(f'Epochs | Avg. Loss: {loss_avg.val()} | Mem. Usage: {torch.cuda.memory_allocated()/torch.cuda.max_memory_allocated()}')

        # reset the loss avg
        loss_avg.reset()

    # save final model
    outfile = '{0}/{1}_{2}_{3}_{4}_{5}.pth'.format(args.store_sample, 'craft' if not args.lara_croft else 'lara-croft', run_name, args.epochs, "FINAL", loss_avg.val())
    print('Saving final model to', outfile)
    torch.save(craft.state_dict(), outfile)

    print('Exiting...')

if __name__ == '__main__':
	main()
