#!/bin/bash

# Job metadata
#SBATCH --job-name='lara croft'
#SBATCH --output="training-logs/lara-croft-run-%j"

# Hardware specs
#SBATCH --partition=dgx
#SBATCH --nodes=1
#SBATCH --gpus=2
#SBATCH --cpus-per-gpu=36
#SBATCH --cpus-per-task=36

# Run time
## time format: <days>-<hours>:<minutes>
#SBATCH --time=1-0:0

# Email notification
#SBATCH --mail-type=END
#SBATCH --mail-user="keysers@msoe.edu"

# command
source /data/csc5651/24-1-group1/dldsp-venv-2/bin/activate
python3 train.py --epochs 50 --lara-croft --cuda-visible-devices "0,1" --preload-emnist
exit $?
