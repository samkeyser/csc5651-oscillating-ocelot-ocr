import os
import numpy as np
import cv2
import sys
import io
import glob
import copy
import torch
import imagesize
# sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
# sys.path.append("..")
# from craft import CRAFT

SAMPLE_SIZE = (600, 400)

def load_ann(image_paths, gt_paths):
    res = []
    for image_path, gt in zip(image_paths, gt_paths):
        item = {}
        item['polys'] = []
        item['tags'] = []
        item['texts'] = []
        item['paths'] = gt
        reader = open(gt, encoding='utf-8').readlines()
        for line in reader:
            parts = line.strip().strip('\xef\xbb\xbf').split(',')
            label = parts[-1]
            if label == '':
                label = parts[-2]
            line = [i.strip('\ufeff') for i in parts]

            # need to rescale these based on original image dimenisons and the sample size!
            orig_width, orig_height = imagesize.get(image_path)
            width_scale = SAMPLE_SIZE[1] / orig_width
            height_scale = SAMPLE_SIZE[0] / orig_height

            x1, y1, x2, y2, x3, y3, x4, y4 = list(map(float, line[:8]))

            x1 *= width_scale
            x2 *= width_scale
            x3 *= width_scale
            x4 *= width_scale

            y1 *= height_scale
            y2 *= height_scale
            y3 *= height_scale
            y4 *= height_scale

            item['polys'].append([[x1, y1], [x2, y2], [x3, y3], [x4, y4]])

            item['texts'].append(label)
            assert len(label) > 0, parts

            if label == '###':
                item['tags'].append(True)
            else:
                item['tags'].append(False)
        item['polys'] = np.array(item['polys'], dtype=np.float32)
        item['tags'] = np.array(item['tags'], dtype=bool)
        item['texts'] = np.array(item['texts'], dtype=str)

        assert item['polys'][:,:,0].max() < SAMPLE_SIZE[1] and item['polys'][:,:,1].max() < SAMPLE_SIZE[0]
        assert item['polys'][:,:,0].min() >= 0 and item['polys'][:,:,1].min() >= 0
        res.append(item)
    return res

class ICDAR2015(object):
    def __init__(self, path, is_training = True):
        self.is_training = is_training
        self.generate_information(path)

    def generate_information(self, path):
        if self.is_training:
            # get path to data
            data_folder = os.path.join(path, 'train')
        else:
            data_folder = os.path.join(path, 'test')

        # get the paths to the relevant files
        self.image_path_list = sorted([image for image in glob.glob(os.path.join(data_folder, "*.jpg"))])
        gt_path_list = sorted([gt for gt in glob.glob(os.path.join(data_folder, "*.txt"))])

        # load annotations
        self.targets = load_ann(self.image_path_list, gt_path_list)

    def len(self):
        return len(self.image_path_list)

# TODO: should standardize on one of these once we know which one actually gets used by the data loader
#       it looks like both of them do???
    def im_read_resize1(self, path):
        img = cv2.imread(path)
        img = cv2.resize(img, SAMPLE_SIZE)
        #img = cv2.resize(img, (2240, 1260))
        return img

    def im_read_resize2(self, path):
        img = cv2.imread(path)
        img = cv2.resize(img, (1280, 720))
        return img

def main():
    # test that we can load training data
    ic15_training = ICDAR2015("/data/csc5651/24-1-group1/datasets/icdar15/", is_training=True)
    print("# of training samples =", len(ic15_training.image_path_list))

    # test that we can load testing data
    ic15_testing = ICDAR2015("/data/csc5651/24-1-group1/datasets/icdar15/", is_training=False)
    print("# of testing samples =", len(ic15_testing.image_path_list))

if __name__ == "__main__":
    main()
