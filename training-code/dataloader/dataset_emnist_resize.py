# library imports
import imgproc
import torch
import scipy.io as sio
import numpy as np
import cv2
import copy
import argparse
import torch.utils.data as data

# import the dataset specific loaders
# NOTE: must remove the dot from this if you want to run this file by itself :shrug:
# from .synthtext import SynthText
from .craft_utils import *
from .emnist import AugmentedEMNIST

# import utilities
import os
import sys
import random
sys.path.append("..")
from utils import interval_list_generate
from craft import CRAFT

# NOTE: this must be run with a GPU!
os.environ['CUDA_VISIBLE_DEVICES'] = '0'

class ImageLoaderAugmentedEMNIST(data.Dataset):
    def __init__(self, args):
        self.args = args
        self.dataset = AugmentedEMNIST(args)

        self.text = self.dataset.sample_metadata["text"]
        self.cor_list = self.dataset.sample_metadata["rotated_characters_bounding_boxes"]
        self.img_path = self.dataset.sample_metadata["img_path"]

    def __len__(self):
        return self.dataset.len()
    
    def __getitem__(self, index):
        # get sample
        img_path = self.img_path[index]
        img_size = (self.args.sample_height, self.args.sample_width)
        img = cv2.imread(img_path)

        # resize
        img, target_ratio, size_heatmap = imgproc.resize_aspect_ratio(img, self.args.canvas_size, interpolation=cv2.INTER_LINEAR, mag_ratio=self.args.mag_ratio)
        ratio_h = ratio_w = 1 / target_ratio

        size_heatmap = size_heatmap[::-1]

        # resize the co-ordinate lists
        resized_cor_list = [adjustResultCoordinates(cor.T, ratio_w, ratio_h).T for cor in self.cor_list]

        # get character GT
        char_label = self.dataset.char_label_generate(self.dataset.gauss_map, size_heatmap, resized_cor_list[index]) # generate heatmaps
        #char_label = self.dataset.char_label_generate(self.dataset.gauss_map, img_size, self.cor_list[index]) # generate heatmaps

        # get affinity boxes
        interval_list = interval_list_generate([self.text[index]])
        interval_label = self.dataset.interval_label_generate(self.dataset.gauss_map, size_heatmap, resized_cor_list[index], interval_list)
        #interval_label = self.dataset.interval_label_generate(self.dataset.gauss_map, img_size, self.cor_list[index], interval_list)

        # preprocessing
        img = imgproc.normalizeMeanVariance(img)

        # clean for output
        img = torch.Tensor(img)
        char_label = torch.Tensor(char_label)
        interval_label = torch.Tensor(interval_label)

        # base char_label and interval_list are [0-255]
        # we need [0-1], so scale down
        #char_label /= 255.0
        #interval_label /= 255.0
        return img, char_label, interval_label

def collate_emnist(batch):
    imgs = []
    char_labels = []
    interval_labels = []
    for sample in batch:
        imgs.append(sample[0])
        char_labels.append(sample[1])
        interval_labels.append(sample[2])
    imgs_stack = torch.stack(imgs, 0)
    char_labels_stack = torch.stack(char_labels, 0)
    interval_labels_stack = torch.stack(interval_labels, 0)
    return imgs_stack.permute(0,3,1,2), char_labels_stack, interval_labels_stack

def main():
    parser = argparse.ArgumentParser(description = 'AugmentedEMNIST')
    parser.add_argument('--batch_size', type=int, default=4, help='input batch size')
    parser.add_argument("--sample_width", default=400, type=int)
    parser.add_argument("--sample_height", default=600, type=int)
    parser.add_argument("--rotation_range", default=45, type=int)
    parser.add_argument('--img_dir', default="/data/csc5651/24-1-group1/training-code/AugmentedEMNIST", type=str)
    parser.add_argument('--dataset_mat', default='/data/csc5651/24-1-group1/training-code/AugmentedEMNIST/dataset.pkl', type=str)
    parser.add_argument('--load', action="store_true")
    parser.add_argument('--canvas_size', default=1280, type=int, help='image size for inference')
    parser.add_argument('--mag_ratio', default=1.5, type=float, help='image magnification ratio')
    args = parser.parse_args()

    args.load = True # WARNING: BLOCKS CREATION OF NEW DATASET FROM THIS SCRIPT!!!

    dataset = ImageLoaderAugmentedEMNIST(args)
    data_loader = data.DataLoader(dataset, args.batch_size, num_workers=1, shuffle=True, collate_fn=collate_emnist)

    # ----- if you want to test view a sample input (2000 elements in dataset) -----
    # test_img, test_cl, test_il = dataset[4]
    # cv2.imwrite("test_emnist_img.png", test_img.numpy())
    # cv2.imwrite("test_emnist_char_label.png", test_cl.numpy())
    # cv2.imwrite("test_emnist_interval_label.png", test_il.numpy())

    print("Data Loader Successfully Set Up!!!")

    model = CRAFT(pretrained=True).cuda()
    for i, batch_samples in enumerate(data_loader):
        if i > 20: break
        batch_img, batch_char_label, batch_interval_label = batch_samples
        batch_img, _ = model(batch_img.cuda())
        print(i, batch_img.shape, batch_char_label.shape, batch_interval_label.shape)

if __name__ == '__main__':
	main()
