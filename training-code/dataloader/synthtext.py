import scipy.io as sio
import numpy as np
import cv2
import copy
import argparse
import sys
import os
sys.path.append("..")
from utils import gauss_normal_generate, cvt2HeatmapImg
from utils import cvt2HeatmapMatrix, point_generate, interval_list_generate

from tqdm import tqdm

class SynthText(object):
    def __init__(self, args):
        self.args = args
        self.mask = np.loadtxt(os.path.join(args.synthtext_img_rootdir, 'skipped-files.lst'), dtype=np.int32)
        self.generate_information()
        
    def generate_information(self):
        # probably generated using https://github.com/ankush-me/SynthText
        self.data = sio.loadmat(self.args.synthtext_gt_mat)

        # filter out the empirically determined bad samples
        for key in ['charBB', 'wordBB', 'imnames', 'txt']:
            self.data[key] = np.delete(self.data[key], self.mask, axis=1)

        char_BB = self.data['charBB'] # character bounding boxes
        self.cor_list = char_BB[0]
        img_txt = self.data['txt']
        self.text = img_txt[0]
        names = self.data['imnames']
        # the third 0 to get the string in list
        self.name = names[0]
        self.gauss_map = gauss_normal_generate(20)

    def len(self):
        return 2000#len(self.data['charBB'][0])

    def im_read_resize(self, path):
        # NOTE: img shape is represented in FS by w x h, but in numpy as h x w
        # because of how array notation swaps x/y
        img = cv2.imread(path)
        if img is None:
            return None, (0,0)
        img_size = (img.shape[0], img.shape[1])
        if img_size[0] > img_size[1]: # TODO: shouldn't this be flipped?
            img = np.rot90(img, -1)
            img_size = (img.shape[0], img.shape[1])
        # NOTE: This man right here is the issue -- if we had any images bigger than 600x400, this will mangle their GT
        resized_img = cv2.resize(img, (600, 400), cv2.INTER_NEAREST)
        return resized_img, resized_img.shape

    def char_label_generate(self, gauss_map, img_size, cor_list):
        # generate the first map with all char box being replaced with gauss map
        h = img_size[0]
        w = img_size[1]
        char_label = np.zeros((h, w))
        char_number = cor_list.shape[2]
        for i in range(char_number):
            x = []
            y = []
            for index in range(4):
                x.append(copy.deepcopy(int(cor_list[0][index][i])))
                y.append(copy.deepcopy(int(cor_list[1][index][i])))
            x_min = max(min(w, min(x)), 0)
            x_max = min(max(x), w)
            y_min = max(min(h, min(y)), 0)
            y_max = min(max(y), h)
            point1 = np.array([[0, 0], [19, 0], [19, 19], [0, 19]], dtype='float32')
            point2 = np.array([[x[0]-x_min, y[0]-y_min], [x[1]-x_min, y[1]-y_min],
                            [x[2]-x_min, y[2]-y_min], [x[3]-x_min, y[3]-y_min]], dtype='float32')
            w_final = x_max - x_min
            h_final = y_max - y_min
            m = cv2.getPerspectiveTransform(point1, point2)

            target = cv2.warpPerspective(gauss_map, m, (w_final, h_final), cv2.INTER_NEAREST)
            for j in range(y_min, y_max):
                for k in range(x_min, x_max):
                    if target[j-y_min][k-x_min] > char_label[j][k]:
                        char_label[j, k] = target[j-y_min][k-x_min]
        if h > w:
            char_label = np.rot90(char_label, -1)
        char_label = cv2.resize(char_label, (w//2, h//2), cv2.INTER_NEAREST)
        char_label = cvt2HeatmapMatrix(char_label)
        return char_label
    
    def interval_label_generate(self, gauss_map, img_size, cor_list, interval_list):
        # generate the first map with all char box being replaced with gauss map
        h = img_size[0]
        w = img_size[1]
        interval_label = np.zeros((h, w))
        char_number = cor_list.shape[2]
        for i in range(char_number-1): # We have N-1 c-c connections for N characters
            if i+1 in interval_list:
                continue
            x1 = []
            y1 = []
            x2 = []
            y2 = []
            for index in range(4):
                x1.append(copy.deepcopy(int(cor_list[0][index][i])))
                y1.append(copy.deepcopy(int(cor_list[1][index][i])))
                x2.append(copy.deepcopy(int(cor_list[0][index][i+1])))
                y2.append(copy.deepcopy(int(cor_list[1][index][i+1])))
            x, y = point_generate(x1, y1, x2, y2)
            x_min = max(min(w, min(x)), 0)
            x_max = min(max(x), w)
            y_min = max(min(h, min(y)), 0)
            y_max = min(max(y), h)
            point1 = np.array([[0, 0], [19, 0], [19, 19], [0, 19]], dtype='float32')
            point2 = np.array([[x[0]-x_min, y[0]-y_min], [x[1]-x_min, y[1]-y_min],
                            [x[2]-x_min, y[2]-y_min], [x[3]-x_min, y[3]-y_min]], dtype='float32')
            w_final = x_max - x_min
            h_final = y_max - y_min
            m = cv2.getPerspectiveTransform(point1, point2)

            target = cv2.warpPerspective(gauss_map, m, (w_final, h_final), cv2.INTER_NEAREST)
            for j in range(y_min, y_max):
                for k in range(x_min, x_max):
                    if target[j-y_min][k-x_min] > interval_label[j][k]:
                        interval_label[j, k] = target[j-y_min][k-x_min]
        if h > w:
            interval_label = np.rot90(interval_label, -1)        
        interval_label = cv2.resize(interval_label, (w//2, h//2), cv2.INTER_NEAREST)
        interval_label = cvt2HeatmapMatrix(interval_label)
        return interval_label

def main():
    parser = argparse.ArgumentParser(description = 'SynthText')
    parser.add_argument('--synthtext_img_rootdir', default='/data/csc5651/24-1-group1/training-code/SynthText/', type=str)
    parser.add_argument('--synthtext_gt_mat', default='/data/csc5651/24-1-group1/training-code/SynthText/gt.mat', type=str)
    args = parser.parse_args()

    skip = []
    synthtext = SynthText(args)
    print(synthtext.len())
    for i in tqdm(range(synthtext.len())):
        try:
            img_path = args.synthtext_img_rootdir + synthtext.name[i][0] 
            #print(i, img_path)

            img, img_size = synthtext.im_read_resize(img_path)
            #print(img_size)

            #if synthtext.cor_list[i][0,:].max() > img_size[0] or synthtext.cor_list[i][1,:].max() > img_size[1]:# or (img_size[0] != 600 or img_size[1] != 400):
            #    print(f"Skipping {i}")
            #    skip.append(i)
            #    continue

            # img has been resized
            char_label = synthtext.char_label_generate(synthtext.gauss_map, img_size, synthtext.cor_list[i])
            interval_list = interval_list_generate(synthtext.text[i])
            interval_label = synthtext.interval_label_generate(synthtext.gauss_map, img_size, synthtext.cor_list[i], interval_list)

            
            #cm_char_label = cv2.resize(cv2.applyColorMap(char_label, cv2.COLORMAP_JET), (img_size[1], img_size[0]), cv2.INTER_NEAREST)
            #img = cv2.addWeighted(cm_char_label, 0.5, img, 0.5, 0)
            #cv2.imwrite(f'test-img-{i}.jpg', img)
        except Exception as e:
            print(f'Skipping {i} because...')
            print(e)
            print()
            skip.append(i)
    print('skipped =', len(skip))
    #with open('skipped-files.lst', 'w') as datafile:
    #    for line in skip:
    #        datafile.write(f"{line}\n")

if __name__ == '__main__':
	main()
