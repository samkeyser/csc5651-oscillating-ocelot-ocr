import scipy.io as sio
import numpy as np
import cv2
import copy
import argparse
import sys
import os
import re
sys.path.append("..") # utils
#sys.path.append("../../data_augment") # data_augment
from data_augment.data_augment import create_handwritten_labeled_sample, get_letter_partitions, get_dataset
from utils import gauss_normal_generate, cvt2HeatmapImg
from utils import cvt2HeatmapMatrix, point_generate, interval_list_generate
import pickle
from tqdm import tqdm

DEBUG_LEVEL = 0

def debug(verbosity, *s):
    if verbosity <= DEBUG_LEVEL:
        print(s)

class AugmentedEMNIST(object):
    def __init__(self, args):
        self.args = args
        
        # get the EMNIST dataset with handwritten characters (also creatig partitions by letter)
        self.emnist_dataset = get_dataset()

        self.eminst_partitions = get_letter_partitions(self.emnist_dataset)
        
        # just a bnuch of tokens to try and create some diversity in the words => if publishing, change to different data
        self.corpus = open("/data/csc5651/24-1-group1/data_augment/bee-movie.txt", "r").read()
        self.tokens = np.unique(re.split(r"[^a-z]+", self.corpus.lower()))
        self.gauss_map_dim = 28
        self.gauss_map = gauss_normal_generate(self.gauss_map_dim)

        if args.load:
            self.load()
        else:
            self.create()
        
    def load(self):
        with open(self.args.dataset_mat, 'rb') as f:
            self.sample_metadata = pickle.load(f)

    def len(self):
        return 2000
    
    def get_text_sample(self, char_width=28, break_prob=0.15):
        words = []
        while True:
            word = np.random.choice(self.tokens)
            if len(" ".join(words + [word])) * char_width < min(self.args.sample_width, self.args.sample_height) and np.random.uniform(0,1) > break_prob:
                words.append(word)
            elif len(words) != 0:
                break
        assert len(" ".join(words)) != 0, f"Fail: {str(' '.join(words))}" 
        return " ".join(words)
            
    def create(self):
        if not os.path.isdir(self.args.img_dir):
            os.mkdir(self.args.img_dir)

        sample_metadata = {"text" : [], "rotation" : [], "dx" : [], "dy" : [], "text_img_bounding_box" : [],  "rotated_characters_bounding_boxes" : [], "img_path" : []}
        for i in tqdm(range(self.len())):
            text_sample = self.get_text_sample()
            note, rotation, (dx, dy), text_img_bb, rot_char_bbs = create_handwritten_labeled_sample(text_sample,
                                                                                                    self.emnist_dataset,
                                                                                                    self.eminst_partitions,
                                                                                                    rotation_range=(-self.args.rotation_range, self.args.rotation_range),
                                                                                                    note_dimensions=(self.args.sample_height,self.args.sample_width))
            
            debug(1, rot_char_bbs.shape)
            debug(2, rot_char_bbs)

            rot_char_bbs_t = np.transpose(rot_char_bbs, (2, 1, 0))

            debug(1, rot_char_bbs_t.shape)
            debug(2, rot_char_bbs_t)
            debug(1, rotation, dx, dy, text_img_bb, type(rot_char_bbs))

            # save sample image
            cv2.imwrite(f"{self.args.img_dir}/sample_{i}.png", note)
            # save sample metadata
            sample_metadata["text"].append(text_sample)
            sample_metadata["rotation"].append(rotation)
            sample_metadata["dx"].append(dx)
            sample_metadata["dy"].append(dy)
            sample_metadata["text_img_bounding_box"].append(text_img_bb)
            sample_metadata["rotated_characters_bounding_boxes"].append(rot_char_bbs_t)
            sample_metadata["img_path"].append(f"{self.args.img_dir}/sample_{i}.png")

            if 1 <= DEBUG_LEVEL: # debugging for generating sample character gauss maps and interval gauss maps
                img_size = (self.args.sample_height, self.args.sample_width)
                
                # get character GT
                char_label = self.char_label_generate(self.gauss_map, img_size, rot_char_bbs_t) # generate heatmaps

                # get affinity boxes
                interval_list = interval_list_generate([text_sample]) # [text_sample] b/c function loops over each part of the text
                print(interval_list)
                interval_label = self.interval_label_generate(self.gauss_map, img_size, rot_char_bbs_t, interval_list)

                # output sample gauss maps
                print(char_label.shape)
                cv2.imwrite(f"{self.args.img_dir}/char_label_{i}.png", char_label)
                print(interval_label.shape)
                cv2.imwrite(f"{self.args.img_dir}/interval_label_{i}.png", interval_label)

        # sio.savemat(self.args.dataset_mat, sample_metadata) # had odd error
        self.sample_metadata = sample_metadata
        with open(self.args.dataset_mat, "wb+") as f:
            pickle.dump(sample_metadata, f)

    def char_label_generate(self, gauss_map, img_size, cor_list):
        # generate the first map with all char box being replaced with gauss map
        h = img_size[0]
        w = img_size[1]
        char_label = np.zeros((h, w))
        char_number = cor_list.shape[2]
        for char_ind in range(char_number):
            x = []
            y = []
            for index in range(4):
                x.append(copy.deepcopy(int(cor_list[0][index][char_ind])))
                y.append(copy.deepcopy(int(cor_list[1][index][char_ind])))

            x_min = max(min(w, min(x)), 0)
            x_max = min(max(x), w)
            y_min = max(min(h, min(y)), 0)
            y_max = min(max(y), h)
            point1 = np.array([[0, 0], [self.gauss_map_dim - 1, 0], [self.gauss_map_dim - 1, self.gauss_map_dim - 1], [0, self.gauss_map_dim - 1]], dtype='float32')
            point2 = np.array([[x[0]-x_min, y[0]-y_min], [x[1]-x_min, y[1]-y_min],
                            [x[2]-x_min, y[2]-y_min], [x[3]-x_min, y[3]-y_min]], dtype='float32')

            w_final = x_max - x_min
            h_final = y_max - y_min
            m = cv2.getPerspectiveTransform(point1, point2)

            target = cv2.warpPerspective(gauss_map, m, (w_final, h_final), cv2.INTER_NEAREST)

            for j in range(y_min, y_max):
                for k in range(x_min, x_max):
                    if target[j-y_min][k-x_min] > char_label[j][k]:
                        char_label[j, k] = target[j-y_min][k-x_min]

        #if h > w:
        #    char_label = np.rot90(char_label, -1)
        #char_label = cv2.resize(char_label, (w//2, h//2), cv2.INTER_NEAREST) # not sure if half-sizing necessary?
        #char_label = cvt2HeatmapMatrix(char_label)
        return char_label
    
    def interval_label_generate(self, gauss_map, img_size, cor_list, interval_list):
        # generate the first map with all char box being replaced with gauss map
        h = img_size[0]
        w = img_size[1]
        interval_label = np.zeros((h, w))
        char_number = cor_list.shape[2]
        for i in range(char_number-1): # We have N-1 c-c connections for N characters
            if i+1 in interval_list:
                continue
            # otw, that means that there is a connecting character

            # 0 and 3 and 1 and 2 # shape of matrix is 2 x 4 x n
            char_0_top = (cor_list[:, 0, char_number-1-(i+1)] + cor_list[:, 3, char_number-1-(i+1)]) / 2 # top left
            char_0_bottom = (cor_list[:, 1, char_number-1-(i+1)] + cor_list[:, 2, char_number-1-(i+1)]) / 2 # bottom left
            char_1_top = (cor_list[:, 0, char_number-1-i] + cor_list[:, 3, char_number-1-i]) / 2 # bottom right
            char_1_bottom = (cor_list[:, 1, char_number-1-i] + cor_list[:, 2, char_number-1-i]) / 2 # top right
            stacked_pts = np.vstack((char_0_top, char_0_bottom, char_1_bottom, char_1_top))
            x = stacked_pts[:, 0].astype("int")
            y = stacked_pts[:, 1].astype("int")

            x_min = max(min(w, min(x)), 0)
            x_max = min(max(x), w)
            y_min = max(min(h, min(y)), 0)
            y_max = min(max(y), h)
            point1 = np.array([[0, 0], [self.gauss_map_dim - 1, 0], [self.gauss_map_dim - 1, self.gauss_map_dim - 1], [0, self.gauss_map_dim - 1]], dtype='float32')
            point2 = np.array([[x[0]-x_min, y[0]-y_min], [x[1]-x_min, y[1]-y_min],
                            [x[2]-x_min, y[2]-y_min], [x[3]-x_min, y[3]-y_min]], dtype='float32')
            w_final = x_max - x_min
            h_final = y_max - y_min
            m = cv2.getPerspectiveTransform(point1, point2)

            target = cv2.warpPerspective(gauss_map, m, (w_final, h_final), cv2.INTER_NEAREST)
            for j in range(y_min, y_max):
                for k in range(x_min, x_max):
                    if target[j-y_min][k-x_min] > interval_label[j][k]:
                        interval_label[j, k] = target[j-y_min][k-x_min]
        #if h > w:
        #    interval_label = np.rot90(interval_label, -1)
        #interval_label = cv2.resize(interval_label, (w//2, h//2), cv2.INTER_NEAREST) # not sure if half-sizing necessary?
        #interval_label = cvt2HeatmapMatrix(interval_label)
        return interval_label
    
def main():
    parser = argparse.ArgumentParser(description = 'AugmentedEMNIST')
    parser.add_argument("--sample_width", default=400, type=int)
    parser.add_argument("--sample_height", default=600, type=int)
    parser.add_argument("--rotation_range", default=45, type=int)
    parser.add_argument('--img_dir', default="/data/csc5651/24-1-group1/training-code/AugmentedEMNIST", type=str)
    parser.add_argument('--dataset_mat', default='/data/csc5651/24-1-group1/training-code/AugmentedEMNIST/dataset.pkl', type=str)
    parser.add_argument('--load', action="store_true")
    args = parser.parse_args()

    data = AugmentedEMNIST(args)

if __name__ == '__main__':
	main()
