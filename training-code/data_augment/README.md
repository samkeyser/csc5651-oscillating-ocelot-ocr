# Data Augmentation Script Usage

## General Uses

### Inputs

- Single image file

### Outputs

- Corresponding augmented output files

### Transformations

- Place image in a larger region (simulate a sheet of notes)

- Apply random rotations to image (input parameter range of rotations)

    - Want the range of rotations to be flexible bc we may overcomplicate the problem space for the task we want to perform (we probably only need the range between both vertical orientations of text)

## Usage

```
place_in_dimensions_random(img, size)
```

Takes in an image (expressed as 3D numpy array) and returns image placed in a white image of size *size*.

```
get_rotated(img, rotation)
```

Takes in an image and returns a rotated version of the image that has a white background.

***Note:*** The function assures that appropriate padding is added, so the rotated image's corners are not clipped.