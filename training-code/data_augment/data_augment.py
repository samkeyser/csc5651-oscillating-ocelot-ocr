import cv2
import matplotlib.pyplot as plt
import numpy as np
from numpy import radians, cos, sin, ceil
from torchvision.datasets import EMNIST
import torch

BASE_PATH = "/data/csc5651/24-1-group1/datasets/emnist/emnist2"

### RANDOM ROTATIONS

def get_rotation_padded(img, rotation):
    """
    Creates a padded form of image so corners are not clipped
    :param img: the image to put padding around
    :param rotation: the angle to rotate (degrees)
    """
    
    height = int(2 * ceil(abs(img.shape[1] / 2 * sin(radians(rotation)) + img.shape[0] / 2 * cos(radians(rotation)))))
    width = int(2 * ceil(abs(img.shape[1] / 2 * cos(radians(rotation)) + img.shape[0] / 2 * sin(radians(rotation)))))
    
    height = max(max(height, img.shape[0]), img.shape[1])
    width = max(max(width, img.shape[1]), img.shape[0])
    
    img_padded = np.zeros((height, width, 3), dtype=np.uint8)
    img_padded[:, :, :] = 255
    
    x_offset = int((img_padded.shape[1] - img.shape[1])/2)
    y_offset = int((img_padded.shape[0] - img.shape[0])/2)
    
    x1, x2 = x_offset, x_offset + img.shape[1]
    y1, y2 = y_offset, y_offset + img.shape[0]
    
    img_padded[y1:y2, x1:x2, :] = img[:, :, :]
    return img_padded

def get_rotated(img, rotation):
    """
    Create a rotated form of image
    :param img: the image to rotate
    :param rotation: the angle to rotate (degrees)
    """
    
    img_padded = get_rotation_padded(img, rotation)
        
    # get properties of padded image
    height, width, _ = img_padded.shape
    size = (width, height)
    center = (width // 2, height // 2)
    scale = 1

    # Create a rotation matrix
    rotation_matrix = cv2.getRotationMatrix2D(center, rotation, scale)

    # Apply the rotation matrix to the first image
    rotated_img = cv2.warpAffine(img_padded, rotation_matrix, size,
                                 flags=cv2.INTER_LINEAR,
                                 borderMode=cv2.BORDER_CONSTANT,
                                 borderValue=(255, 255, 255))

    return rotated_img

def get_rotation_bounding_box(img, rotation):
    """
    Given an image, returns the bounding box for the rotation image
    :param img: the image being rotated
    :param rotation: the angle to rotate
    """
    
    padded_height = int(2 * ceil(abs(img.shape[1] / 2 * sin(radians(rotation)) + img.shape[0] / 2 * cos(radians(rotation)))))
    padded_width = int(2 * ceil(abs(img.shape[1] / 2 * cos(radians(rotation)) + img.shape[0] / 2 * sin(radians(rotation)))))
    
    padded_height = max(max(padded_height, img.shape[0]), img.shape[1])
    padded_width = max(max(padded_width, img.shape[1]), img.shape[0])
    
    bounding_box = np.zeros((4, 2))
    d_theta = radians(rotation)
    add_x = padded_width / 2
    add_y = padded_height / 2
    for i, (quad_x, quad_y) in enumerate(((1, 1), (-1, 1), (-1, -1), (1, -1))):
        x = img.shape[1] / 2 * quad_x
        y = img.shape[0] / 2 * quad_y
        r = np.sqrt(x ** 2 + y ** 2)
        theta = np.arctan2(y, x)
        bounding_box[i, 0] = r * cos(theta - d_theta) + add_x
        bounding_box[i, 1] = r * sin(theta - d_theta) + add_y
    return bounding_box


### RANDOM PLACEMENTS

def place_in_dimensions(img, size, location):
    """
    Places image in a white image at a certain location
    :param img: the image to place
    :param size: the dimensions of the returned image (height, width)
    :param location: the location to place the top left corner of image (row, col)
    """
    
    height, width, _ = img.shape
    new_img = np.zeros((*size, 3), dtype=np.uint8)
    new_img[:, :, :] = 255
    row, col = location
    new_img[row:row+height, col:col+width, :] = img[:, :, :]
    return new_img
    
def place_in_dimensions_random(img, size):
    """
    Places image in a white image at a random location 
    that would include entire image
    :param img: the image to place
    :param size: the dimensions of the returned image (height, width)
    """
    
    assert img.shape[0] <= size[0] and img.shape[1] <= size[1]
    height, width, _ = img.shape
    new_height, new_width = size
    valid_row = np.random.randint(new_height - height + 1)
    valid_col = np.random.randint(new_width - width + 1)
    return place_in_dimensions(img, size, (valid_row, valid_col)), valid_col, valid_row

### EMNIST DATASETS

def get_dataset(path=BASE_PATH):
    """
    Gets the EMNIST dataset into a folder. Downloads if does currently exist
    """
    dataset = EMNIST(root=path, split="byclass", train=True, download=True)
    return dataset

def get_class_mappings():
    return [chr(i) for i in range(ord('0'),ord('9')+1)] + [chr(i) for i in range(ord('a'),ord('z')+1)] + [chr(i) for i in range(ord('A'),ord('Z')+1)]
    
def get_letter_partitions(dataset):
    """
    Groups all images by class for easier retrieval (as a dictionary)
    :param dataset: EMNIST torch dataset
    """
    partitions = {}
    classes = get_class_mappings()
    for i, val in enumerate(sorted(dataset.train_labels.unique().numpy().tolist())):
        partitions[classes[i]] = dataset.train_data[dataset.train_labels == val]
    return partitions

### HANDWRITTEN SAMPLES

def bw_2_rgb(bw):
    """
    Converts black and white (1D) image to RGB (3D)
    :param bw: the black and white image to convert
    """
    
    res = np.zeros((*bw.shape, 3), dtype=np.uint8)
    pix_vals = 255 - bw.numpy()
    res[:, :, 0] = pix_vals
    res[:, :, 1] = pix_vals
    res[:, :, 2] = pix_vals
    return res
    
def create_handwritten_string(s, dataset, partitions):
    """
    Creates a handwritten string from a text input
    :param s: string to convert
    """
    
    character_imgs = []
    is_character = []
    for c in s:
        if c in partitions.keys():
            img = torch.transpose(partitions[c][np.random.randint(len(partitions[c]))], 0, 1)
            character_imgs.append(img)
            is_character.append(True)
        else:
            character_imgs.append(torch.ones(28, 28, dtype=torch.uint8))
            is_character.append(False)
    bounding_boxes = np.zeros((len(character_imgs), 4, 2))
    for i in range(len(character_imgs)):
        bounding_boxes[i, 0, :] = [i * 28, 0] # top left
        bounding_boxes[i, 1, :] = [i * 28, 27] # bottom left
        bounding_boxes[i, 2, :] = [(i + 1) * 28 - 1, 27] # bottom right
        bounding_boxes[i, 3, :] = [(i + 1) * 28 - 1, 0] # top right
    return torch.cat(character_imgs, dim=1), bounding_boxes[list(reversed(is_character))] # don't fully know why, but reversed is correct :)

def get_padded_dims(img, rotation):
    """
    Gets the expected padding size of an image given the rotation that will be performed.
    :param img: image to rotate
    :param rotation: the number of degrees to rotate
    """
    
    height = int(2 * ceil(abs(img.shape[1] / 2 * sin(radians(rotation)) + img.shape[0] / 2 * cos(radians(rotation)))))
    width = int(2 * ceil(abs(img.shape[1] / 2 * cos(radians(rotation)) + img.shape[0] / 2 * sin(radians(rotation)))))
    
    height = max(max(height, img.shape[0]), img.shape[1])
    width = max(max(width, img.shape[1]), img.shape[0])
    
    return width, height

def rotate_char_boxes_around_point(x, y, points, rotation):
    """
    Rotates several bounding boxes a certain number of degrees around a certain point
    :param x: x-coordinate of point to rotate around
    :param y: y-coordinate of point to rotate around
    :param points: the rectangles to rotate (shape (n-polygons, 4, 2))
    :param rotation: the number of degrees to rotate by
    """
        
    d_theta = radians(rotation)
    dxs = x - points[:, :, 0]
    dys = y - points[:, :, 1]
    rs = np.sqrt(dxs ** 2 + dys ** 2)
    thetas = np.arctan2(dys, dxs)
    points_rotated = points.copy()
    points_rotated[:, :, 0] = rs * np.cos(thetas - d_theta) + x
    points_rotated[:, :, 1] = rs * np.sin(thetas - d_theta) + y
    return points_rotated

def create_handwritten_labeled_sample(s, dataset, partitions, rotation_range=(0,360), note_dimensions=(600, 400)):
    """
    Main augmentation function. Takes in a string and produces a randomly augmented
    handwritten note with the character-level bounding boxes as polygons.
    :param s: string to convert
    :param dataset: EMNIST dataset
    :param partitions: dictionary paritioning EMNIST dataset by character
    :param rotatation_range: range of angles for random rotation to be at
    :param note_dimensions: the size of the note to produce with a given sample
    :return:
        note - the actual image (training input)
        rotation - the amount rotated
        (dx, dy) - the amount randomly shifted within note
        text_img_bb - the overarching bounding box of the text (shape (4, 2))
        rot_char_bbs - the bounding boxes for each of the characters (shape (n-chars, 4, 2))
    """
    
    # creating image
    text_img, char_bbs = create_handwritten_string(s, dataset, partitions)
    text_img = bw_2_rgb(text_img)
    
    # random rotation
    rotation = np.random.randint(*rotation_range)
    text_img_bb = get_rotation_bounding_box(text_img, rotation)
    padded_width, padded_height = get_padded_dims(text_img, rotation)
    width_diff = padded_width - text_img.shape[1]
    height_diff = padded_height - text_img.shape[0]
    
    # random position
    note, dx, dy = place_in_dimensions_random(get_rotated(text_img, rotation), note_dimensions)
    
    # character bounding boxes
    rot_char_bbs = rotate_char_boxes_around_point(text_img.shape[1] / 2, text_img.shape[0] / 2, char_bbs, rotation)
    rot_char_bbs[:, :, 0] += dx + width_diff / 2
    rot_char_bbs[:, :, 1] += dy + height_diff / 2
    
    text_img_bb[:, 0] += dx
    text_img_bb[:, 1] += dy
    
    return note, rotation, (dx, dy), text_img_bb, rot_char_bbs