import torch
import argparse
from dataloader.dataset_synthtext import ImageLoaderSynthText, collate_synthtext
from dataloader.dataset_emnist import ImageLoaderAugmentedEMNIST, collate_emnist

# Define argparser
parser = argparse.ArgumentParser()

parser.add_argument('--batch-size', type=int, default=4, help='input batch size')
### synthtext
parser.add_argument('--synthtext-img-rootdir', default='/data/csc5651/24-1-group1/training-code/SynthText/', type=str)
parser.add_argument('--synthtext-gt-mat', default='/data/csc5651/24-1-group1/training-code/SynthText/gt.mat', type=str)
### augmented emnist
parser.add_argument("--sample_width", default=400, type=int)
parser.add_argument("--sample_height", default=600, type=int)
parser.add_argument("--rotation_range", default=45, type=int)
parser.add_argument('--img_dir', default="/data/csc5651/24-1-group1/training-code/AugmentedEMNIST", type=str)
parser.add_argument('--dataset_mat', default='/data/csc5651/24-1-group1/training-code/AugmentedEMNIST/dataset.pkl', type=str)
parser.add_argument('--preload-emnist', action="store_true", dest="load") # set the dest to be backwards compatible with how the dataloader_emnist is set up

## parse out argumetns
args = parser.parse_args()

# load the datasets
## load synthtext
synthtext_dataset = synthtext_dataloader = None
print('Loading SynthText dataloader')
synthtext_dataset = ImageLoaderSynthText(args)
assert synthtext_dataset
synthtext_dataloader = torch.utils.data.DataLoader(synthtext_dataset, args.batch_size, num_workers=2, shuffle=False, collate_fn=collate_synthtext)

emnist_dataset = emnist_dataloader = None
print('Loading EMNIST dataloader')
emnist_dataset = ImageLoaderAugmentedEMNIST(args)
assert emnist_dataset
emnist_dataloader = torch.utils.data.DataLoader(emnist_dataset, args.batch_size, num_workers=1, shuffle=True, collate_fn=collate_emnist)

_, synthtext_gt, _ = next(iter(synthtext_dataloader))
_, emnist_gt, _ = next(iter(emnist_dataloader))

print('synthtext_gt =')
print(synthtext_gt.max(), synthtext_gt.min())
print()
print('========')
print()
print('emnist_gt =')
print(emnist_gt.max(), emnist_gt.min())
