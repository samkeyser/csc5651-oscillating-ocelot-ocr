#!/bin/bash

# Job metadata
#SBATCH --job-name='vanilla-CRAFT'
#SBATCH --output="training-logs/vanilla-CRAFT-run-%j"

# Account stuff
#SBATCH --account=undergrad_research

# Hardware specs
#SBATCH --partition=dgx
#SBATCH --nodes=1
#SBATCH --gpus=8
#SBATCH --cpus-per-gpu=8

# Run time
## time format: <days>-<hours>:<minutes>
#SBATCH --time=1-0:0

# Email notification
#SBATCH --mail-type=END
#SBATCH --mail-user="johnstonem@msoe.edu"

# command
source /data/csc5651/24-1-group1/dldsp-venv-2/bin/activate
#python3 train.py --epochs 1 --resume-target-approximator-from-checkpoint --cuda-visible-devices "0,1" --preload-emnist --update-target-approximator --resume-from-checkpoint
#python3 train2.py --epochs 1 --resume-target-approximator-from-checkpoint --cuda-visible-devices "0,1" --preload-emnist --update-target-approximator --resume_from_checkpoint
#python3 train.py --epochs 1 --resume-target-approximator-from-checkpoint --cuda-visible-devices "0,1" --preload-emnist --update-target-approximator --resume-from-checkpoint --lr 1e-20 --synthtext-freq 0 --icdar2015-freq 0 --emnist-freq 1
#python3 train.py --epochs 1 --resume-target-approximator-from-checkpoint --cuda-visible-devices "0,1" --preload-emnist --update-target-approximator --resume-from-checkpoint --synthtext-freq 0 --icdar2015-freq 0 --emnist-freq 1
#python3 train2.py --epochs 1 --resume-target-approximator-from-checkpoint --cuda-visible-devices "0,1" --preload-emnist --update-target-approximator --resume-from-checkpoint --lr 1e-20

#python3 train.py --epochs 100 --resume-target-approximator-from-checkpoint --cuda-visible-devices "0,1,2,3" --preload-emnist --synthtext-freq 0 --icdar2015-freq 0 --emnist-freq 1 --lara-croft
#python3 shk-train-5.py --epochs 200 --resume-target-approximator-from-checkpoint --cuda-visible-devices "0,1,2,3" --preload-emnist --synthtext-freq 0 --icdar2015-freq 0 --emnist-freq 1 --lara-croft --use-sig --batch-size 32 --pre-model /data/csc5651/24-1-group1/trained-models/lara-croft_happy_visvesvaraya_100_FINAL_0.pth --resume-from-checkpoint

python3 shk-train-5.py --epochs 100 --resume-target-approximator-from-checkpoint --cuda-visible-devices "0,1,2,3,4,5,6,7" --preload-emnist --synthtext-freq 0 --icdar2015-freq 0 --emnist-freq 1 --lara-croft --use-sig --batch-size 64
