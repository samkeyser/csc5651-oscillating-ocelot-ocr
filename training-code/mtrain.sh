#!/bin/bash

# Job metadata
#SBATCH --job-name='vanilla-CRAFT'
#SBATCH --output="training-logs/vanilla-CRAFT-run-%j"

# Hardware specs
#SBATCH --partition=dgx
#SBATCH --nodes=1
#SBATCH --gpus=2
#SBATCH --cpus-per-gpu=36
#SBATCH --cpus-per-task=36

# Run time
## time format: <days>-<hours>:<minutes>
#SBATCH --time=1-0:0

# Email notification
#SBATCH --mail-type=END
#SBATCH --mail-user="johnstonem@msoe.edu"

# command
source /data/csc5651/24-1-group1/dldsp-venv-2/bin/activate
# 128132
python3 train.py --epochs 50 --lr 0.001 --batch-size 16 --resume-target-approximator-from-checkpoint --cuda-visible-devices "0,1" --preload-emnist

#python3 train.py --epochs 50 --batch-size 32 --resume-target-approximator-from-checkpoint --cuda-visible-devices "0,1" --preload-emnist
