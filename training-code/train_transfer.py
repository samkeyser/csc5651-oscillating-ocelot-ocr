import torch
import scipy.io as sio
import numpy as np
import cv2
import copy
import argparse
import os
import time
import torch.optim as optim
import copy
import glob
from tqdm import tqdm
from names_generator import generate_name

from craft import CRAFT
from utils import thresh_calculate, k_b_calculate, pixel_test, label_convert 
from utils import watershed_algorithm, label_generate, label_collate, averager, calculate_confidence_score

from dataloader.dataset_ic15 import ImageLoaderICDAR2015, collate_icdar2015, batch_random_augmentation
from dataloader.dataset_synthtext import ImageLoaderSynthText, collate_synthtext
from dataloader.dataset_emnist import ImageLoaderAugmentedEMNIST, collate_emnist

from parser import get_parser

# Define argparser
parser = get_parser()

## parse out argumetns
args = parser.parse_args()

print('--------------[CHOSEN ARGS]--------------')
print(args)
print('-----------------------------------------')

# method for float equality
def epsilon_equality(a, b, epsilon=0.0001):
    return abs(a-b) < epsilon

# make sure that the chosen dataset frequencies sum to 1!
summed_freq = (args.synthtext_freq + args.icdar2015_freq + args.emnist_freq)
assert epsilon_equality(1.0, summed_freq), f'Dataset frequencies do not sum to 1, {summed_freq}'

# make sure that the sample store directory exists
print('Creating store directory', args.store_sample)
os.system('mkdir -p {0}'.format(args.store_sample))

# make sure that registry file exists
print('Creating registry file', f'{args.store_sample}/.registry')
os.system('touch {0}/.registry'.format(args.store_sample))

# load the datasets
## load synthtext
synthtext_dataset = synthtext_dataloader = None
if not epsilon_equality(args.synthtext_freq, 0.0): # no sense loading if we never want to use
    print('Loading SynthText dataloader')
    synthtext_dataset = ImageLoaderSynthText(args)
    assert synthtext_dataset
    synthtext_dataloader = torch.utils.data.DataLoader(synthtext_dataset, args.batch_size, num_workers=2, shuffle=False, collate_fn=collate_synthtext)

## load ICDAR2015
icdar2015_dataset = icdar2015_dataloader = None
if not epsilon_equality(args.icdar2015_freq, 0.0): # no sense loading if we never want to use
# TODO: refactor away from the config dictionary...
    print('Loading ICDAR2015 dataloader')
    config={'is_training':True, 'image_path': args.icdar2015_img_rootdir}
    icdar2015_dataset = ImageLoaderICDAR2015(config)
    assert icdar2015_dataset
    icdar2015_dataloader = torch.utils.data.DataLoader(icdar2015_dataset, args.batch_size, num_workers=2, shuffle=True, collate_fn=collate_icdar2015)

emnist_dataset = emnist_dataloader = None
if not epsilon_equality(args.emnist_freq, 0.0):
    print('Loading EMNIST dataloader')
    emnist_dataset = ImageLoaderAugmentedEMNIST(args)
    assert emnist_dataset
    emnist_dataloader = torch.utils.data.DataLoader(emnist_dataset, args.batch_size, num_workers=1, shuffle=True, collate_fn=collate_emnist)

# figure out if we're on CUDA or cpu
device = 'cuda' if torch.cuda.is_available() else 'cpu'
print('Running on', device)

# set the number of GPUs available
os.environ['CUDA_VISIBLE_DEVICES'] = args.cuda_visible_devices
print('GPUs assigned =', args.cuda_visible_devices)

# loss function
print('Initializing loss function')
criterion = torch.nn.MSELoss(reduction='sum').to(device)

# pull in the model
## we use a second version of craft as a target approximator for gt labels on datasets w/o char-level BBs
print('Initializing target approximator')
target_approximator = torch.nn.DataParallel(CRAFT(pretrained=True).to(device)) # note: pretrained refers to the vgg16 basenet, NOT CRAFT!

## actual model which we will be training
print('Initializing CRAFT')
craft_net = CRAFT(pretrained=True, use_LOTM=args.lara_croft, use_sig=args.use_sigmoid).to(device)
craft = torch.nn.DataParallel(craft_net)

## if we're resuming training, load model from checkpoint
if args.resume_from_checkpoint:
    print('loading pretrained model from %s for main model' % args.pre_model)
    craft.load_state_dict(torch.load(args.pre_model), strict=False)

net = craft
modules = list(craft.named_modules())

# Print Model Summary
print(modules[0])
print()

for name, module in modules:
    if type(module).__name__ not in ["ReLU", "BatchNorm2d", "Conv2d", "MaxPool2d", "DataParallel", "CRAFT"]:
        named_parameters = list(module.named_parameters()) # actual weights
        print("\n" + type(module).__name__ + f" (Requires Grad: {len(named_parameters)})" + " -> ", end="")
    else:
        print(type(module).__name__ + " ", end="")

print()
print()
print(craft_net.conv_cls)

for param in craft.parameters():
    param.requires_grad = False # does not do gradient descent through these layers

# just ensuring that the final convolutional layer requires gradient descent (transfer learning)
conv_cls_params = list(craft_net.conv_cls.named_parameters())
conv_cls_params[-2][1].requires_grad = True # weights
conv_cls_params[-1][1].requires_grad = True # bias
print()
for name, param in conv_cls_params:
    print(name, param.requires_grad)

print()
num_params_requiring_grad = 0
for param in craft.parameters():
    num_params_requiring_grad += int(param.requires_grad)
print("Parameters Requiring Grad:", num_params_requiring_grad)

# exit()

## we may also want to load target approximtor, either for training or so we can use a pretrained model
if args.resume_target_approximator_from_checkpoint:
    print('loading pretrained model from %s for target approximator' % args.pre_model_target_approximator)
    target_approximator.load_state_dict(torch.load(args.pre_model_target_approximator), strict=False)

# other training thigs
loss_avg = averager()
optimizer = optim.Adam(craft.parameters(), lr=args.lr)

# training functions
## Synth
def train_batch_synthtext(data):
    craft.train()
    img, char_label, interval_label = data
    img = img.to(device)
    char_label = char_label.to(device) # region score
    interval_label = interval_label.to(device) # affinity score

    img.requires_grad_()
    optimizer.zero_grad()
    preds, _ = craft(img)
    cost_char = criterion(preds[:,:,:,0], char_label) / args.batch_size
    cost_interval = criterion(preds[:,:,:,1], interval_label) / args.batch_size
    cost = cost_char + cost_interval

    cost.backward()
    optimizer.step()
    return cost.item()

## ICDAR2015
def train_batch_icdar2015(data):
    target_approximator.eval()
    craft.train()
    img, information = data

    img = img.to(device)
    label_output, _ = target_approximator(img)
    char_label, interval_label = label_collate(label_output, information)
    #return torch.ones((1,)) # const around 4055
    
    # img, char_label, interval_label = batch_random_augmentation(img, char_label, interval_label)
    # img = img.permute(0, 3, 1, 2).to(device)
    char_label = torch.FloatTensor(char_label).to(device)
    interval_label  = torch.FloatTensor(interval_label).to(device)
    #return torch.ones((1,)) # const around 4055

    img.requires_grad_()
    optimizer.zero_grad()
    preds, _ = craft(img)

    s_conf = calculate_confidence_score(information, preds.detach())
    s_conf = torch.from_numpy(s_conf).to(device)
    #return torch.ones((1,)) # cost around 7202

    # batch x height x width x 2
    cost_char = (torch.sum(s_conf * (preds[:,:,:,0] - char_label)**2, dim=0) / args.batch_size).sum()
    cost_interval = (torch.sum(s_conf * (preds[:,:,:,1] - interval_label)**2, dim=0) / args.batch_size).sum()
    #return torch.ones((1,)) # cost around 7211

    cost = cost_char + cost_interval
    cost.backward(retain_graph=False)
    optimizer.step()
    return cost.item() # holding const around 8138

def train_on(epoch, dataloader, train_batch_fn, dataloader_name, run_name):
    batch_bar = tqdm(range(len(dataloader)), desc=f"{dataloader_name} batches")
    for i, data in zip(batch_bar,dataloader):
        # data = train_iter.next()
        cost = train_batch_fn(data)
        loss_avg.add(cost)

        # do checkpointing
        if i % args.save_interval == 0:
            torch.save(craft.state_dict(), '{0}/{1}_{2}_{3}_{4}_{5}.pth'.format(args.store_sample, 'craft' if not args.lara_croft else 'lara-croft', run_name, epoch, i, loss_avg.val()))
            
            # update the target approximator to be our current trained model
            if args.update_target_approximator:
                target_approximator = copy.deepcopy(craft)

        # update loss avg
        batch_bar.set_description(f'{dataloader_name} batches | Avg. Loss: {loss_avg.val()} | Mem. Usage: {torch.cuda.memory_allocated()/torch.cuda.max_memory_allocated()}')

        #if i % args.displayInterval == 0:
        #    print('[%d/%d][%d/%d] lr: %.6f Loss: %f Time: %f s' %
        #        (epoch, args.epochs, i, length, optimizer.param_groups[0]['lr'], loss_avg.val(), time.time()-time0))
        #    loss_avg.reset()

def get_name():
    run_name = generate_name()
    with open(os.path.join(args.store_sample, '.registry'), 'r') as datafile:
        names = set(datafile.readlines())
        while run_name in names:
            run_name = generate_name()
    return run_name

def register_name(run_name):
    with open(os.path.join(args.store_sample, '.registry'), 'a+') as datafile:
        datafile.write('{0}\n'.format(run_name))


def main():
    # generate a unique name for this run
    if not args.run_name:
        run_name = get_name()

    print("run name =", run_name)

    # register the run name so we don't use it again
    print('Registering run name')
    register_name(run_name)

    # log which model config we're running with!
    if args.lara_croft:
        print("Using LARA CROFT model!")
    else:
        print("using CRAFT model!")

                    
    # defaults -> synthtext = 0.25, icdar15 = 0.4, emnist = 0.35
    sampling_dist = [args.synthtext_freq, args.icdar2015_freq, args.emnist_freq]
    dataloaders = [
        (synthtext_dataloader, train_batch_synthtext, "SYNTHTEXT"),
        (icdar2015_dataloader, train_batch_icdar2015, "ICDAR2015"),
        (emnist_dataloader, train_batch_synthtext, "EMNIST"), # we can use the same training method as synthtext b/c we have character level gt!
    ]

    epoch_bar = tqdm(range(args.epochs), desc='epochs')
    for epoch in epoch_bar:
        # choose a dataloader according to predetermined distribution
        chosen_idx = np.random.choice(len(dataloaders), p=sampling_dist)
        chosen_dataloader, chosen_train_batch, chosen_name = dataloaders[chosen_idx]

        # perform training with chosen dataset
        train_on(epoch, chosen_dataloader, chosen_train_batch, chosen_name, run_name)

        # update progress bar
        epoch_bar.set_description(f'Epochs | Avg. Loss: {loss_avg.val()} | Mem. Usage: {torch.cuda.memory_allocated()/torch.cuda.max_memory_allocated()}')

        # reset the loss avg
        loss_avg.reset()

    # save final model
    outfile = '{0}/{1}_{2}_{3}_{4}_{5}.pth'.format(args.store_sample, 'craft' if not args.lara_croft else 'lara-croft', run_name, args.epochs, "FINAL", loss_avg.val())
    print('Saving final model to', outfile)
    torch.save(craft.state_dict(), outfile)

    print('Exiting...')

if __name__ == '__main__':
	main()
