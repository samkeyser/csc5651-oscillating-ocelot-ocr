# Getting CRAFT Working

## Working Interactively
### Getting a Node
```bash
salloc --nodes=1 --ntasks=1 --mem=128G --time=01:00:00 --cpus-per-task=16 --gpus 1
```
Adjust `time`, `cpus-per-task`, and `gpus` accordingly

Use `squeue` to learn what node was allocated for your job. Use `ssh` to access the node
```bash
ssh <dh-node>
```

### Virtualenv
```bash
source dldsp-venv-2/bin/activate
```

Enviroment was created as follows:
```bash
pip install torch==0.4.1.post2 -f https://download.pytorch.org/whl/torch_stable.html
pip install -r requirements.txt
```

where `requirements.txt` is the one stored in `CRAFT-pytorch`. **This was modified from the `requirements.txt` provided by the authors**.

### Running the Model
Finally, the pretrained CRAFT model can be tested by running the following command, assuming you are in the `CRAFT-pytorch` directory:
```bash
python3 test.py --trained_model=../trained-models/craft_mlt_25k.pth --test_folder=../datasets/smoke-test
```

Substitute `trained_model`, `test_folder` as desired to test different model configs over different datasets.

The results will be stored in `CRAFT-pytorch/result`.


# Getting Tesseract working

Tesseract is an open source OCR library provided by Google.
We are borrowing an implementation from online.

## Getting the singularity container of a working Tessearct

```bash
pull docker://tesseractshadow/tesseract4re
```

## Running Tesseract

To run tesseract, we need to use the singularity container.

Importantly, we need to bind the directory of our repo to the virtual environment so it can access test files and write output.
Below is an example of using a test handwriting dataset file.
```bash
singularity exec -B /data/csc5651/24-1-group1:/mnt tesseract4re_latest.sif tesseract /mnt/datasets/handwriting-recognition/test_v2/test/TEST_12345.jpg /mnt/TEST_12345
```
