#!/bin/bash

# Job metadata
#SBATCH --job-name='testing-CRAFT'
#SBATCH --output="shk-testing-logs/test-CRAFT-run-%j"

# Hardware specs
#SBATCH --partition=dgx
#SBATCH --nodes=1
#SBATCH --gpus=1
#SBATCH --cpus-per-task=36

# Run time
## time format: <days>-<hours>:<minutes>
#SBATCH --time=1-0:0

# Email notification
#SBATCH --mail-type=END
#SBATCH --mail-user="johnstonem@msoe.edu"

# command
source /data/csc5651/24-1-group1/dldsp-venv-2/bin/activate

TF="/data/csc5651/24-1-group1/datasets/EMNIST-smoke-test"
declare -a TM=( "/data/csc5651/24-1-group1/trained-models/lara-croft_happy_visvesvaraya_100_FINAL_0.pth" "/data/csc5651/24-1-group1/trained-models/lara-croft_clever_wing_100_FINAL_0.pth")
declare -a RF=( "./happy-visvesvaraya-130479/" "./clever-wing-130472/")
declare -a LC=( "--lara-croft" "--lara-croft" )
declare -a SIG=( "--use-sigmoid" "" )

len=${#TM[@]}

for i in {0..1} #$(seq 0 $len)
do
	echo "RUSULT FOLDER: ----------------------------------- ${RF[$i]}"
	#rm "${RF[$i]}"/*
   #  Run the python script
    python3 test.py --test_folder $TF --trained_model "${TM[$i]}" --result_folder "${RF[$i]}" ${LC[$i]} ${SIG[$i]}

    # Run the tesseract
    cd "${RF[$i]}"
    for f in *.jpg
    do 
        b=`basename "$f" .jpg`
        singularity exec -B .:/mnt /data/csc5651/24-1-group1/tesseract4re_latest.sif tesseract "/mnt/$f" "/mnt/$b"
    done
    cd ..
done
