"""
Important: This script is not intended to be run independently unless you have already 
run test.py to generate predictions with a model. This script will be called correctly
if you use run-test-jtk.sh (or any derivation from that script, which calls this script
at the end of generating outputs using Tesseract).
"""

import argparse
import os
import glob
import re
import abydos.distance as abd
from itertools import permutations
import json
import numpy as np

parser = argparse.ArgumentParser(description='CRAFT Text Detection')
parser.add_argument('--folder', type=str, help='folder to search through')

args = parser.parse_args()
model_name = args.folder.replace('/', '').replace(".", "")

ids = []
for file in glob.glob(f"{args.folder}*_mask.txt"):
    id = re.search(r"(\d+)_mask.txt", file).groups()[0]
    ids.append(id)

results = {}
for id in ids:
    results[id] =[]
    for file in glob.glob(f"{args.folder}*{id}_bb_*.txt"):
        found_text = open(file, "r").read().strip()
        if len(found_text) != 0:
            results[id].append(found_text)

# getting the best ordering of groups -> lowest levenshtein distance from group permutations
# (don't know which order model would give, but we could probably programmatically create a way of
# getting the order that is the most likely)
lev_dist = abd.Levenshtein()
labels = json.loads(open("labels.json", "r").read())
preds = {}

for id, words in results.items():
    best_result_score = 1
    best_result = " ".join(words)
    label = labels[id]
    for perm in permutations(words):
        poss_result = " ".join(perm)
        dist_score = lev_dist.dist(label.lower(), poss_result.lower())
        if dist_score < best_result_score:
            best_result = poss_result
            best_result_score = dist_score

    best_result = best_result.replace("\n", "")
    with open(f"{args.folder}res_sample_{id}_regions_grouped.txt", "w+") as f:
        print("Expected: %20s, Predicted: %20s | Comparison: %20s = %-20s" % (label, 
                                                                              best_result if len(best_result) != 0 else '<N/A>',
                                                                              label.lower(),
                                                                              best_result.lower()))
        f.write(best_result.strip())
        preds[id] = {"text": best_result.strip(), "dist" : best_result_score, "label" : labels[id]}

has_base = len(glob.glob(f"{args.folder}*_base.txt")) != 0

# getting predictions, using tesseract only
tesseract_preds = {}
for id in ids:
    tesseract_only = open(f"{args.folder}res_sample_{id}_{'base' if has_base else 'mask'}.txt", "r").read().replace("\n", "").strip()
    dist_score = lev_dist.dist(label.lower(), tesseract_only)
    tesseract_preds[id] = {"text": tesseract_only, "dist" : dist_score, "label" : labels[id]}

# same character percent as described in CRAFT paper
def get_char_percent(pred, label):
    diff = abs(len(pred) - len(label))
    diff = min(len(label), diff)
    return (len(label) - diff) / len(label)

def get_metrics(model_preds):
    lev_dists = list(map(lambda x: x["dist"], model_preds.values()))
    text_detections = list(map(lambda x: int(len(x["text"]) != 0), model_preds.values()))
    char_detection_pcts = list(map(lambda x: get_char_percent(x["text"], x["label"]), model_preds.values()))
    return {
        "lev_dists" : lev_dists,
        "text_detections" : text_detections,
        "char_detection_pcts" : char_detection_pcts
    }

metrics = get_metrics(preds)
tesseract_metrics = get_metrics(tesseract_preds)

# sample by sample metrics -> not outputted into files + might be excessive with larger smoke test
print()
print("%20s|%30s|%20s|%15s|%10s|%45s|%20s|%15s|%10s" % ("Actual", "Tesseract", "Norm. Lev. Dist.", "Text Found?", "Char %", f"Tesseract + {model_name}", "Norm. Lev. Dist.", "Text Found?", "Char %"))
print("%20s+%30s+%20s+%15s+%10s+%45s+%20s+%15s+%10s" % ("-" * 20, "-" * 30, "-" * 20, "-" * 15, "-" * 10, "-" * 45, "-" * 20, "-" * 15, "-" * 10))
for id in ids:
    tesseract_only = open(f"{args.folder}res_sample_{id}_mask.txt", "r").read().strip().replace("\n", "")
    print("%20s|%30s|%20s|%15s|%10s|%45s|%20s|%15s|%10s" % (
        labels[id], 
        tesseract_only if len(tesseract_only) != 0 else '<N/A>', 
        "%.3f" % tesseract_metrics["lev_dists"][int(id)],
        "%d" % tesseract_metrics["text_detections"][int(id)],
        "%.3f" % tesseract_metrics["char_detection_pcts"][int(id)],
        preds[id]["text"] if len(preds[id]["text"]) != 0 else '<N/A>',
        "%.3f" % metrics["lev_dists"][int(id)],
        "%d" % metrics["text_detections"][int(id)],
        "%.3f" % metrics["char_detection_pcts"][int(id)],
        ))

overall_results = {}
overall_results["Tesseract"] = {}
overall_results[f"Tesseract + {model_name}"] = {}

print()  
print("%30s|%30s|%45s" % ("Metric", "Tesseract", f"Tesseract + {model_name}"))
print("%30s+%30s+%45s" % ("-" * 30, "-" * 30, "-" * 45))

# text found
print("%30s|%30s|%45s" % ("Text Found %", 
                          "%.3f" % np.mean(tesseract_metrics["text_detections"]),
                          "%.3f" % np.mean(metrics["text_detections"])
                          ))
overall_results["Tesseract"]["Text Found %"] = np.mean(tesseract_metrics["text_detections"])
overall_results[f"Tesseract + {model_name}"]["Text Found %"] = np.mean(metrics["text_detections"])

# lev dist
print("%30s|%30s|%45s" % ("Avg. Norm. Lev. Dist.", 
                          "%.3f" % np.mean(tesseract_metrics["lev_dists"]),
                          "%.3f" % np.mean(metrics["lev_dists"])))
overall_results["Tesseract"]["Avg. Norm. Lev. Dist."] = np.mean(tesseract_metrics["lev_dists"])
overall_results[f"Tesseract + {model_name}"]["Avg. Norm. Lev. Dist."] = np.mean(metrics["lev_dists"])

# char found %
print("%30s|%30s|%45s" % ("Avg. Char. Found %",
                          "%.3f" % np.mean(tesseract_metrics["char_detection_pcts"]),
                          "%.3f" % np.mean(metrics["char_detection_pcts"])))
overall_results["Tesseract"]["Avg. Char. Found %"] = np.mean(tesseract_metrics["char_detection_pcts"])
overall_results[f"Tesseract + {model_name}"]["Avg. Char. Found %"] = np.mean(metrics["char_detection_pcts"])
print()

# output to results file
with open(f"{args.folder}overall_eval_metrics.json", "w+") as f:
    f.write(json.dumps(overall_results, indent=4))
    print("Saved Results: %s" % f"{args.folder}overall_eval_metrics.json")