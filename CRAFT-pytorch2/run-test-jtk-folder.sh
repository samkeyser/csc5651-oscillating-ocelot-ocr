#!/bin/bash

# Job metadata
#SBATCH --job-name='testing-CRAFT'
#SBATCH --output="test-CRAFT-run-%j.out"

# Hardware specs
#SBATCH --partition=dgx
#SBATCH --nodes=1
#SBATCH --gpus=1
#SBATCH --cpus-per-task=36

# Run time
## time format: <days>-<hours>:<minutes>
#SBATCH --time=1-0:0

# Email notification
#SBATCH --mail-type=END
#SBATCH --mail-user="keanej@msoe.edu"

# command
source /data/csc5651/24-1-group1/dldsp-venv-2/bin/activate

TF="/data/csc5651/24-1-group1/datasets/EMNIST-smoke-test"
declare -a TM=("/data/csc5651/24-1-group1/training-code/store/craft_strange_hypatia_1_FINAL_0.pth")
declare -a RF=("./priceless_meitner_130778_copy/")
declare -a LC=("")
declare -a SIG=("")

len=${#TM[@]}

for i in {0..0} #$(seq 0 $len)
do
    # run evaluation script
    python3 eval_jtk.py --folder "${RF[$i]}"
done
