# CSC5651-oscillating-ocelot-ocr

![](https://64.media.tumblr.com/adfa4448fb6d0ef428fc572357d2df26/tumblr_inline_pitliytSJv1sk4fj1_250.gif)

## Overview
Oscillating Ocelot is an attempt to improve OCR for scene text detection on difficult handwritten samples. Or spoken plainly, identify the regions of an image containing handwritten characters, words, or phrases. Existing solutions perform poorly in the case that these samples are rotated, curvy, or otherwise augmented. We build a custom model, LARA CROFT, based on [CRAFT](https://arxiv.org/abs/1904.01941) and [ContourNet](https://arxiv.org/abs/2004.04940).

## Getting started
Refer to [SETUP.md](SETUP.md) for getting the code running.
